def main():
    number_string = "1, 2, 3, oops, 5"
    split_string = number_string.split(",")
    int_list = []
    for token in split_string:
        try:
            int_list.append(int(token))
        except ValueError:
            print("Bad token:", token)
    print("Int list:", int_list)

if __name__ == "__main__":
    main()