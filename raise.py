def main():
    bar(-1)
    
def bar(foo):
    if foo < 0:
        raise ValueError(f"Foo must be > 0: {foo}")
        
    print("This only prints values >= 0", foo)

if __name__ == "__main__":
    main()
