import time

def main():
    process_a_million_files()
    
def process_a_million_files():
    # Just process five fake files.
    for i in range(5):
        try:
            process_single_file(i)
        except KeyboardInterrupt:
            raise
        except:
            print("Encountered an error!")
        
def process_single_file(i):
    print("Processing: ", i)
    if i == 3:
        raise ValueError(f"Iteration: {i} is bad.")
    time.sleep(2)

if __name__ == "__main__":
    main()
