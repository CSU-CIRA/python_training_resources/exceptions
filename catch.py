def main():
    try:
        bad_code()
    except ZeroDivisionError:
        print("Oops we divided by zero.")
        
    print("We got to the end of the program.")
    
def bad_code():
    foo = 5/0
    print("We got to the end of the bad_code function.")

if __name__ == "__main__":
    main()
