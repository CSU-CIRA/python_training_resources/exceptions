def main():
    try:
        print("Nothing wrong here.")
    except:
        print("This will never be printed.")
    else:
        print("Nothing went wrong, so we'll print this.")
    finally:
        print("This will be printed no matter what.")

if __name__ == "__main__":
    main()
