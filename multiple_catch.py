def main():
    try:
        bad_open()
    except ZeroDivisionError:
        print("Oops we divided by zero.")
    except FileNotFoundError:
        print("Oops we tried opening a file that doesn't exist.")
    
    try:
        bad_open()
        bad_code()
    except (ZeroDivisionError, FileNotFoundError):
        print("We respond to the two errors using the same code")
    
    print("We got to the end of the program.")
    
def bad_code():
    foo = 5/0
    print("We got to the end of the bad_code function.")

def bad_open():
    open("this_file_does_not_exist.txt", "r")
    print("We got to the end of the bad_open function.")

if __name__ == "__main__":
    main()
